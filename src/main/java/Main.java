import DemoService.Http;
import com.mashape.unirest.http.exceptions.UnirestException;

public class Main {
    public static void main(String[] args) {
        try {
            new Http().execute();
        } catch (UnirestException e) {
            e.printStackTrace();
        }
    }
}
