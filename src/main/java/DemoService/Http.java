package DemoService;

import com.mashape.unirest.http.HttpResponse;

import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import java.util.HashMap;
import java.util.Map;

public class Http {
    public void execute() throws UnirestException {
        System.out.println("==========POST==========");
        HttpResponse<String> response = Unirest.post("http://httpbin.org/post")
                .header("accept", "application/json")
                .queryString("apiKey", "123")
                .asString();
        print(response);


        System.out.println("\n==========GET==========");
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("q", "London");
        parameters.put("units", "metric");
        parameters.put("appid", "241de9349721df959d8800c12ca4f1f3");
        HttpResponse<JsonNode> jsonResponse = Unirest.get("http://api.openweathermap.org/data/2.5/weather")
                .header("accept", "application/json")
                .queryString(parameters)
                .asJson();
        print(jsonResponse);

        System.out.println("\n==========POST==========");
        jsonResponse = Unirest.post("http://httpbin.org/post")
                .header("accept", "application/json")
                .body("{\"param0\":\"Kirill\", \"param1\":\"password\"}")
                .asJson();
        print(jsonResponse);

        System.out.println("\n==========GET==========");
        response = Unirest.get("https://www.avito.ru/udmurtiya")
                .header("accept", "application/json")
                .queryString("q", "ps4")
                .asString();
        print(response);
    }


    private void print(HttpResponse<?> response) {
        System.out.println("Status: " + response.getStatus() + " " + response.getStatusText() +
                            "\nHeaders: " + response.getHeaders() +
                            "\nBody: " + response.getBody());
    }

}



